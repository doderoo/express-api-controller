import {
  Document,
  Model,
} from 'mongoose';

export interface IApiTimestamp {
  created: {
    at: Date;
    by: string;
  };
  updated: {
    at: Date;
    by: string;
  };
}

export type ApiTimeStampModel = Model<IApiTimestamp>;
export type ApiTimeStampDocument = IApiTimestamp & Document;
