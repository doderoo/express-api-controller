import {
  Request,
} from 'express';
import {
  FilterQuery,
} from 'mongoose';

import {
  IApiMeta,
} from './IApiMeta';
import {
  ApiDocument,
} from './IApiModel';
import {
  ApiUserDocument,
} from './IApiUser';

export interface IApiRequest extends Request {
  modelQuery?: {
    total: FilterQuery<unknown>;
    offset: number;
    limit: number;
  };
  meta?: IApiMeta;
  data?: ApiDocument[];
  stats?: any;
  model?: ApiDocument;
  user?: ApiUserDocument;
  dateRange?: any;
}
