import {
  Document,
  FilterQuery,
  Model,
} from 'mongoose';

import {
  ParseQueryFunction,
} from './IApiQuery';
import {
  IApiTimestamp,
} from './IApiTimestampModel';

export interface IApiModel {
  timestamps: IApiTimestamp;
  mark: {
    deleted: boolean;
  };
}

export type ApiDocument = Document & IApiModel;

export interface ApiModel<T extends (IApiModel & Document), K = unknown> extends Model<T> {
  parseQuery: ParseQueryFunction<T>;
  statistics: (query: FilterQuery<unknown>) => Promise<{[ key: string ]: K[]}>;
}
