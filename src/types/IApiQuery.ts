import {
  FilterQuery,
  PopulateOptions,
} from 'mongoose';

export type ApiSortQuery = Record<string, -1 | 1>;
export type ApiSelectQuery = Record<string, boolean>;

export interface IApiQuery {
  deleted?: string | boolean;
  filter?: string | null;
  limit?: string | number;
  offset?: string | number;
  populate?: PopulateOptions[] | [] | null;
  q?: string | null;
  select?: string | null;
  sort?: string | null;
}
export interface IApiParsedQuery<T = unknown> {
  [ key: string ]: undefined | null | string | number | boolean | Partial<Record<string, any>>;
  _q?: string;
  limit: number;
  offset: number;
  q: FilterQuery<T>;
  select: ApiSelectQuery;
  sort: ApiSortQuery | null; // should not be null
  total: FilterQuery<T>;
  filter?: Record<string, string>;
  populate?: PopulateOptions[];
  deleted?: boolean;
}

export type ParseQueryFunction<T = unknown> = (query: IApiParsedQuery<T>) => IApiParsedQuery<T>;
