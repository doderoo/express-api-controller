export interface ErrorField {
  kind: string;
  message: string;
  name: string;
  path: string;
  properties: any;
  value: string;
}
export interface IApiError {
  id: string;
  message: string;
  fields: Record<string, ErrorField>;
  errors: IApiError[];
}
