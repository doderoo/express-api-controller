import {
  ApiDocument,
  ApiModel,
  IApiModel,
} from './IApiModel';


export interface IUserModel extends IApiModel {
  username: string;
}

export type ApiUserDocument = ApiDocument & IUserModel;

export type ApiUserModel = ApiModel<ApiUserDocument>;

