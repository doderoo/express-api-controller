"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const isString_1 = require("./helpers/isString");
class ApiController {
    constructor(model) {
        this.model = model;
    }
    respondServerError(res, error) {
        return res.status(500).json({ error });
    }
    respondNotFound(id, res, modelName) {
        const error = {
            id: 'notFound',
            message: `${modelName} ${id} does not exist`,
            fields: {},
            errors: [],
        };
        return res.status(404).json({
            error,
        });
    }
    respondInvalidId(res) {
        const error = {
            id: 'invalidId',
            message: 'Invalid id',
            fields: {},
            errors: [],
        };
        return res.status(400).json({
            error,
        });
    }
    respondModelMissingError(res) {
        const error = {
            id: 'modelMissing',
            message: 'the model is missing in the request',
            fields: {},
            errors: [],
        };
        return res.status(400).json({
            error,
        });
    }
    respondDeletionError(res, err) {
        const error = {
            id: 'delete',
            message: err.message,
            fields: {},
            errors: [],
        };
        return res.status(400).json({
            error,
        });
    }
    respondValidationError(err, res, next) {
        if (err.name === 'ValidationError') {
            const error = {
                id: 'validationError',
                message: err.message,
                fields: err.errors,
                errors: [],
            };
            return res.status(400).json({
                error,
            });
        }
        if (err.code === 11000) {
            return res.status(400).json({
                error: {
                    id: 'duplicate',
                    message: `${this.model.modelName} already exists`,
                },
            });
        }
        else {
            return next(err);
        }
    }
    apiResponse(req, res, _next) {
        const hasMetaError = req.meta && req.meta.error;
        let status = 200;
        if (hasMetaError) {
            status = 500;
        }
        return res.status(status).json({ meta: req.meta, data: req.data });
    }
    populateMeta(req, _res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            var _a;
            const qTotal = ((_a = req.modelQuery) === null || _a === void 0 ? void 0 : _a.total) || {};
            return this.model.countDocuments(qTotal)
                .then((total) => {
                var _a, _b;
                const offset = ((_a = req.modelQuery) === null || _a === void 0 ? void 0 : _a.offset) || 0;
                const limit = ((_b = req.modelQuery) === null || _b === void 0 ? void 0 : _b.limit) || 100;
                req.meta = {
                    total: total,
                    count: req.data ? req.data.length : 0,
                    offset: (0, isString_1.isString)(offset) ? parseInt(offset, 10) : offset,
                    limit: (0, isString_1.isString)(limit) ? parseInt(limit, 10) : limit,
                };
                return next();
            })
                .catch((err) => next(err));
        });
    }
    hasModel(model) {
        var _a;
        return typeof model !== 'undefined'
            && model !== null
            && ((_a = model.constructor) === null || _a === void 0 ? void 0 : _a.name) === 'model'
            && mongoose_1.Types.ObjectId.isValid(model._id);
    }
}
exports.default = ApiController;
//# sourceMappingURL=api.controller.js.map