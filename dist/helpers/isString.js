"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isString = void 0;
function isString(value) {
    return typeof value !== 'undefined' && typeof value === 'string';
}
exports.isString = isString;
//# sourceMappingURL=isString.js.map