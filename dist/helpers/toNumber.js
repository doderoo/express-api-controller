"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.toNumber = void 0;
const isString_1 = require("./isString");
function toNumber(value) {
    return (0, isString_1.isString)(value) ? parseInt(value, 10) : value;
}
exports.toNumber = toNumber;
//# sourceMappingURL=toNumber.js.map