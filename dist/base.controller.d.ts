/// <reference types="mongoose/types/aggregate" />
/// <reference types="mongoose/types/callback" />
/// <reference types="mongoose/types/collection" />
/// <reference types="mongoose/types/connection" />
/// <reference types="mongoose/types/cursor" />
/// <reference types="mongoose/types/document" />
/// <reference types="mongoose/types/error" />
/// <reference types="mongoose/types/expressions" />
/// <reference types="mongoose/types/helpers" />
/// <reference types="mongoose/types/middlewares" />
/// <reference types="mongoose/types/indexes" />
/// <reference types="mongoose/types/models" />
/// <reference types="mongoose/types/mongooseoptions" />
/// <reference types="mongoose/types/pipelinestage" />
/// <reference types="mongoose/types/populate" />
/// <reference types="mongoose/types/query" />
/// <reference types="mongoose/types/schemaoptions" />
/// <reference types="mongoose/types/schematypes" />
/// <reference types="mongoose/types/session" />
/// <reference types="mongoose/types/types" />
/// <reference types="mongoose/types/utility" />
/// <reference types="mongoose/types/validation" />
/// <reference types="mongoose/types/virtuals" />
/// <reference types="mongoose/types/inferschematype" />
import { ObjectId } from 'bson';
import { NextFunction, Request, Response } from 'express';
import { PopulateOptions } from 'mongoose';
import ApiController from './api.controller';
import { ApiDocument, ApiModel, IApiRequest } from './types';
import { ApiSortQuery, IApiParsedQuery } from './types/IApiQuery';
export type ServerResponsePromise = Promise<void | Response<any, Record<string, any>>>;
declare abstract class BaseController<T extends (ApiDocument)> extends ApiController<T> {
    protected filters: string[];
    constructor(model: ApiModel<T>);
    index(req: IApiRequest, _res: Response, next: NextFunction): Promise<void>;
    read(req: IApiRequest, res: Response, _next: NextFunction): ServerResponsePromise;
    create(req: IApiRequest, res: Response, next: NextFunction): ServerResponsePromise;
    update(req: IApiRequest, res: Response, next: NextFunction): ServerResponsePromise;
    softDelete(req: IApiRequest, res: Response, _next: NextFunction): ServerResponsePromise;
    delete(req: IApiRequest, res: Response, _next: NextFunction): ServerResponsePromise;
    findById(req: IApiRequest, res: Response, next: NextFunction, id: string | number | ObjectId, _urlParam?: any, populate?: PopulateOptions[]): ServerResponsePromise;
    stats(req: IApiRequest, res: Response, next: NextFunction): ServerResponsePromise;
    statsResponse(req: IApiRequest, res: Response, _next: NextFunction): Response;
    statistics(req: IApiRequest, res: Response, next: NextFunction): ServerResponsePromise;
    parseDateRange(req: IApiRequest, _res: Response, next: NextFunction, _id: string, _urlParam: string): void;
    processQuery(query: Request['query'], defaultQuery: Readonly<IApiParsedQuery>): IApiParsedQuery;
    parseSort(sort?: string | null): ApiSortQuery | null;
    parseFilter(filterQuery?: string | null): Record<string, string>;
    parsePagination(value: string, defaultValue: string | number): number;
    parseQuery(query: Partial<IApiParsedQuery>): IApiParsedQuery;
}
export default BaseController;
//# sourceMappingURL=base.controller.d.ts.map