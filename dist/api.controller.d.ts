import { ObjectId } from 'bson';
import { NextFunction, Response } from 'express';
import { ApiDocument, ApiModel } from './types/IApiModel';
import { IApiRequest } from './types/IApiRequest';
declare abstract class ApiController<T extends ApiDocument> {
    protected model: ApiModel<T>;
    constructor(model: ApiModel<T>);
    respondServerError(res: Response, error: any): Response;
    respondNotFound(id: string | number | ObjectId, res: Response, modelName: string): Response;
    respondInvalidId(res: Response): Response;
    respondModelMissingError(res: Response): Response;
    respondDeletionError(res: Response, err: Error): Response;
    respondValidationError(err: any, res: Response, next: NextFunction): Response | void;
    apiResponse(req: IApiRequest, res: Response, _next: NextFunction): Response;
    populateMeta(req: IApiRequest, _res: Response, next: NextFunction): Promise<void>;
    protected hasModel<K extends ApiDocument>(model: ApiDocument | null | undefined): model is K;
}
export default ApiController;
//# sourceMappingURL=api.controller.d.ts.map