/// <reference types="mongoose/types/aggregate" />
/// <reference types="mongoose/types/callback" />
/// <reference types="mongoose/types/collection" />
/// <reference types="mongoose/types/connection" />
/// <reference types="mongoose/types/cursor" />
/// <reference types="mongoose/types/document" />
/// <reference types="mongoose/types/error" />
/// <reference types="mongoose/types/expressions" />
/// <reference types="mongoose/types/helpers" />
/// <reference types="mongoose/types/middlewares" />
/// <reference types="mongoose/types/indexes" />
/// <reference types="mongoose/types/models" />
/// <reference types="mongoose/types/mongooseoptions" />
/// <reference types="mongoose/types/pipelinestage" />
/// <reference types="mongoose/types/populate" />
/// <reference types="mongoose/types/query" />
/// <reference types="mongoose/types/schemaoptions" />
/// <reference types="mongoose/types/schematypes" />
/// <reference types="mongoose/types/session" />
/// <reference types="mongoose/types/types" />
/// <reference types="mongoose/types/utility" />
/// <reference types="mongoose/types/validation" />
/// <reference types="mongoose/types/virtuals" />
/// <reference types="mongoose/types/inferschematype" />
import { Document, FilterQuery, Model } from 'mongoose';
import { ParseQueryFunction } from './IApiQuery';
import { IApiTimestamp } from './IApiTimestampModel';
export interface IApiModel {
    timestamps: IApiTimestamp;
    mark: {
        deleted: boolean;
    };
}
export type ApiDocument = Document & IApiModel;
export interface ApiModel<T extends (IApiModel & Document), K = unknown> extends Model<T> {
    parseQuery: ParseQueryFunction<T>;
    statistics: (query: FilterQuery<unknown>) => Promise<{
        [key: string]: K[];
    }>;
}
//# sourceMappingURL=IApiModel.d.ts.map