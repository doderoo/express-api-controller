export * from './IApiError';
export * from './IApiMeta';
export * from './IApiModel';
export * from './IApiQuery';
export * from './IApiRequest';
export * from './IApiTimestampModel';
export * from './IApiUser';
//# sourceMappingURL=index.d.ts.map