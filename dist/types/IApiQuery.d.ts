/// <reference types="mongoose/types/aggregate" />
/// <reference types="mongoose/types/callback" />
/// <reference types="mongoose/types/collection" />
/// <reference types="mongoose/types/connection" />
/// <reference types="mongoose/types/cursor" />
/// <reference types="mongoose/types/document" />
/// <reference types="mongoose/types/error" />
/// <reference types="mongoose/types/expressions" />
/// <reference types="mongoose/types/helpers" />
/// <reference types="mongoose/types/middlewares" />
/// <reference types="mongoose/types/indexes" />
/// <reference types="mongoose/types/models" />
/// <reference types="mongoose/types/mongooseoptions" />
/// <reference types="mongoose/types/pipelinestage" />
/// <reference types="mongoose/types/populate" />
/// <reference types="mongoose/types/query" />
/// <reference types="mongoose/types/schemaoptions" />
/// <reference types="mongoose/types/schematypes" />
/// <reference types="mongoose/types/session" />
/// <reference types="mongoose/types/types" />
/// <reference types="mongoose/types/utility" />
/// <reference types="mongoose/types/validation" />
/// <reference types="mongoose/types/virtuals" />
/// <reference types="mongoose/types/inferschematype" />
import { FilterQuery, PopulateOptions } from 'mongoose';
export type ApiSortQuery = Record<string, -1 | 1>;
export type ApiSelectQuery = Record<string, boolean>;
export interface IApiQuery {
    deleted?: string | boolean;
    filter?: string | null;
    limit?: string | number;
    offset?: string | number;
    populate?: PopulateOptions[] | [] | null;
    q?: string | null;
    select?: string | null;
    sort?: string | null;
}
export interface IApiParsedQuery<T = unknown> {
    [key: string]: undefined | null | string | number | boolean | Partial<Record<string, any>>;
    _q?: string;
    limit: number;
    offset: number;
    q: FilterQuery<T>;
    select: ApiSelectQuery;
    sort: ApiSortQuery | null;
    total: FilterQuery<T>;
    filter?: Record<string, string>;
    populate?: PopulateOptions[];
    deleted?: boolean;
}
export type ParseQueryFunction<T = unknown> = (query: IApiParsedQuery<T>) => IApiParsedQuery<T>;
//# sourceMappingURL=IApiQuery.d.ts.map