## 4.0.2

* 6bb62c1 fix querying

## 4.0.1

* c2f1a04 add error message to query parsing
* 6e10bfd Merge branch 'simplification-one' into 'master'
* f70abd4 Upgraded all libraries and simplified code, typescript 5 and async/await

## 4.0.0

* migrated everything to the latest packages
* introduced async/await where possible
* migrated to typescript 5
* tested on prod application
* 028170f add comment
* ab771e9 checkin dist
* 9d9c546 fix deletion and response
* f68372a add model query
* 9effcbc add query string store place
* 0c52989 check in dist
* e4db6b2 attempt to fix problem with findById
* 2438d44 fix stats query

## 4.0.0-beta-3

* Fix invalid use of countDocument

## 4.0.0-beta-2

* updating and testing on real repo
* 9f45043 fix typo
* 9f5f81c checkin dist
* 34eee5f checkin vscode settings
* 8975017 tweak types
* 99a737d update packages
* 7eea576 checkin dist
* 9faaae0 tweak types
* c0bf71d update dist
* b6cb4b2 clean imports
* b8f1dc9 add new eslint rules
* 5dcae01 checkin dist
* 0d4d761 improve type safety
* de420aa clean up
* 9afbb3f attempt for simplification
* 71819dc Merge branch 'upgrade' into 'master'
* 61f1a77 Upgrade all dependencies, use promises over callbacks from mongoose

## 4.0.0-beta-1

* updating all dependencies and rewriting to promesque mongoose behavior

## 3.0.1

* 7ed65f5 update bundles
* 633825d bump version
* aabf4f1 upgrade dependencies
* ced59d5 remove deprecation
* 12bdfd2 simplify types
* 7f98966 fix types and use promises
* 7f1ab43 fix tests
* 3af7487 upgrade dependencies

## 3.0.0

* upgrades all dependencies and adjusts breaking changes
* e5a0bb5 fixes after upgrade
* d37f2c6 attempt to update
* d05fe0e fix docker image version on gitlab

## 3.0.0-1

* 41a7f2b fix spec
* d81cdb0 add npmignore

## 3.0.0-0

upgrade all dependencies and try to use more meaningful types
* 59a80b3 fix types
* c950c93 increase linting rules and auto fix
* 3e66641 fix specs
* 26a8b47 fix linting
* 67d4da4 add eslint
* 7b77cca tweak a little bit. Mongoose types get out of hand to properly type

## 2.1.8

* 7976f5b limit sort keys to 1 and -1
* 87ba882 add default sort to date descending

## 2.1.7

* 7b4e97c patch query parser for sort, if sorted twice

## 2.1.6

* 8b7c067 fix type for select

## 2.1.5

* fd0ef28 fix select

## 2.1.4

* 965148e downgrade express types
* 8b8e835 remove declare before depracted tslint rule

## 2.1.3

* e27377e use mongoose filter type
* 16e2ca9 upgrade dependencies

## 2.1.2

* (Breaking) change timestamp export naming
* fix transpiling
* extend ApiError with typed error field extend ApiTimestampModel

## 2.1.1

* make timestamps.update mandatory

## 2.1.0

* make timestamp model more loose typed, to allow empty updated and at
* extend api error to allow nested errors

## 2.0.3



## 2.0.2

* expose helper types
* remove invalid field from parsed query

## 2.0.1
* stricter types for parse query

## 2.0.0
### breaking
* renamed package

## 2.0.0-0

### breaking
- change to typescript
- add typed exports
- add testing
- overall cleanup
